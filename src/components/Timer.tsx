import React, { useState, useEffect, useRef, useCallback } from 'react';
import { interval, Subscription } from 'rxjs';
import Gauge from "./Gauge";

const Timer = () => {
  const [elapsedTime, setElapsedTime] = useState(0);
  const [duration, setDuration] = useState(60);
  const [isRunning, setIsRunning] = useState(false);
  const timerRef = useRef<Subscription | null>(null);

  const handleTick = useCallback(() => {
    setElapsedTime(e => e + 1);
    if (elapsedTime + 1 >= duration) {
      setIsRunning(false);
      setElapsedTime(0);
    }
  }, [duration, elapsedTime]);

  useEffect(() => {
    if (isRunning) {
      timerRef.current = interval(1000).subscribe(handleTick);
    }
    return () => timerRef.current?.unsubscribe();
  }, [isRunning, handleTick]);

  const handleSliderChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    const value = +event.target.value;
    if (!isNaN(value)) {
      setDuration(value);
    }
  }, []);

  const handleStart = useCallback(() => {
    setElapsedTime(0);
    setIsRunning(true);
  }, []);

  const handleReset = useCallback(() => {
    setElapsedTime(0);
    setIsRunning(false);
  }, []);

  return (
      <div className="flex flex-col items-center justify-center p-4">
        <Gauge value={elapsedTime / duration} />
        <p className="text-lg font-semibold my-2">Elapsed Time: {elapsedTime}s</p>
        <div className="my-2">
          <input
              id="slider"
              type="range"
              min="0"
              max="300"
              value={duration}
              onChange={handleSliderChange}
              className="w-full"
              disabled={isRunning}
          />
          <div className="text-center text-sm">{`${duration} s`}</div>
        </div>
        {!isRunning ? (
            <button className="px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-700 transition duration-300" onClick={handleStart}>Start</button>
        ) : (
            <button className="px-4 py-2 bg-red-500 text-white rounded hover:bg-red-700 transition duration-300" onClick={handleReset}>Reset</button>
        )}
      </div>
  );
};

export default Timer;
