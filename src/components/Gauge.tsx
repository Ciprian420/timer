import { GaugeProps } from "../models/gauge-models";

const Gauge = ({ value }: GaugeProps) => {
    const normalizedValue = Math.min(Math.max(value, 0), 1);
    const filledWidth = `${normalizedValue * 100}%`;

    return (
        <div className="h-5 w-2/3 bg-gray-300 rounded overflow-hidden border border-gray-300 my-2">
            <div className="h-full bg-green-500 transition-width duration-500 ease-out" style={{ width: filledWidth }}></div>
        </div>
    );
};

export default Gauge;
